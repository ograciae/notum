if(!global.hasOwnProperty('db')){

  var mongoose = require('mongoose');
  var dbName = 'expressTest'

  mongoose.connect('mongodb://localhost/'+dbName);

  global.db = {
    mongoose: mongoose,
    User: require('./users')(mongoose),
  };
}

module.exports = global.db;
