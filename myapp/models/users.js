module.exports = function(mongoose){
  var Schema = mongoose.Schema;

  var UserSchema = new Schema({
    nombre: String,
    apellido: String,
    telefono: String,
    direccion: String,
    isActive: Boolean
  });

  (UserSchema.methods.age = function(){
    return ~~((Date.now() - this.birthdate) / (31557600000));
  })

  return mongoose.model('usuarios',UserSchema);
}
